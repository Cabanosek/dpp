FROM python:3.8-slim


WORKDIR /myapp

COPY . .

RUN pip install -r requirements.txt

EXPOSE 5000

CMD flask run
